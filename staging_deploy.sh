#!/bin/bash
echo "Getting Started"
set -x

# Variables
BUILD_DIR="/home/deploy/build/contentful"
DEPLOY_DIR="/home/contentful"
SERVICE_NAME="contentful"

# Pull the latest changes from the repository
cd $BUILD_DIR
/usr/bin/git checkout -f staging
/usr/bin/git pull origin staging

# Cleanup step
echo "Cleaning up old packages and build artifacts..."
rm -rf node_modules
rm -rf build
npm cache clean --force

# Run the build process
echo "Installing dependencies..."
npm ci --verbose || npm install --verbose || { echo "npm install failed"; exit 1; }

echo "Building the project..."
npm run build || { echo 'npm run build failed'; exit 1; }

# Run the local compile.sh script
echo "Running compile.sh..."
chmod +x ./compile.sh
./compile.sh
RC=$?
if [[ $RC != 0 ]]; then
    echo "compile.sh failed! Aborting..."
    exit 1
fi

echo "Build process completed successfully"
echo "Built files are located in $BUILD_DIR/build"
echo "Manual intervention required:"
echo "1. Copy files from $BUILD_DIR/build to $DEPLOY_DIR"
echo "2. Set appropriate permissions on $DEPLOY_DIR"
echo "3. Restart the $SERVICE_NAME service"