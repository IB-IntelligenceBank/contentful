#!/bin/bash
set -x  # Enable debug mode

echo "Starting compile.sh"
echo "Current directory: $(pwd)"
echo "Contents of build directory:"
ls -la build

# Add any additional compilation steps here if needed

echo "Finished compile.sh"