#!/bin/bash
echo "Getting Started"
set -x

# Variables
BUILD_DIR="/home/deploy/build/contentful"
BRANCH="master"

# Pull the latest changes from the repository
cd "$BUILD_DIR" || { echo "Cannot cd into $BUILD_DIR"; exit 1; }
/usr/bin/git checkout -f "$BRANCH"
/usr/bin/git pull origin "$BRANCH"

# Cleanup step
echo "Cleaning up old packages and build artifacts..."
rm -rf node_modules
rm -rf build
npm cache clean --force

# Run the build process
echo "Installing dependencies..."
npm ci --verbose || npm install --verbose || { echo "npm install failed"; exit 1; }

echo "Building the project..."
npm run build || { echo 'npm run build failed'; exit 1; }

# Copy the built files to the deployment directory
echo "Copying build files..."
sudo /usr/bin/rm -rfv /home/contentful/contentful
sudo /usr/bin/cp -rv "$BUILD_DIR" /home/contentful/
sudo /usr/bin/chown -Rv contentful:contentful /home/contentful/

# Restart the service
echo "Restarting $SERVICE_NAME..."
sudo /usr/bin/systemctl stop contentful
sudo /usr/bin/systemctl start contentful

echo "Deployment completed successfully"